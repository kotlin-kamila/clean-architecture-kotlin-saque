package com.kamila.saque.boundary.usecase.domain

import java.math.BigDecimal

class SaqueInput(
    val amount: BigDecimal,
    val qrCode: String
)