package com.kamila.saque.boundary.usecase.service

import com.kamila.saque.boundary.usecase.domain.SaqueInput
import com.kamila.saque.boundary.usecase.domain.SaqueReturnInput

interface SaqueServiceInput {
    fun registerSaque(saqueInput: SaqueInput): SaqueReturnInput
}
