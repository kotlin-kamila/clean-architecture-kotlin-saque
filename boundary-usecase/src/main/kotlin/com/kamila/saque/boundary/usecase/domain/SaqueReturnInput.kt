package com.kamila.saque.boundary.usecase.domain

import java.math.BigDecimal
import java.time.LocalDateTime

data class SaqueReturnInput(
    val amount: BigDecimal,
    val qrCodeUuid: String,
    val createDate: LocalDateTime
)

