package com.kamila.saque.boundary.database

import com.kamila.saque.entity.QrCode

interface QrCodeDatabase {
    fun saveQrCode(qrCode: QrCode): QrCode
}