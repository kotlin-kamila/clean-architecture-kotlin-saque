package com.kamila.saque.usecase.mapper

import com.kamila.saque.entity.QrCode
import com.kamila.saque.boundary.usecase.domain.SaqueReturnInput
import java.math.BigDecimal
import java.time.LocalDateTime

fun QrCode.toSaqueReturnInput(amount: BigDecimal) = SaqueReturnInput(
    amount = amount,
    qrCodeUuid = fullText,
    createDate = LocalDateTime.now()
)
