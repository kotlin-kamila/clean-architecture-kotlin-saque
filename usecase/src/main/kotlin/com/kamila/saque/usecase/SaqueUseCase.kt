package com.kamila.saque.usecase

import com.kamila.saque.commons.exception.ApplicationException
import com.kamila.saque.commons.exception.NotFoundException
import com.kamila.saque.boundary.database.QrCodeDatabase
import com.kamila.saque.entity.toQrCode
import com.kamila.saque.boundary.usecase.domain.SaqueInput
import com.kamila.saque.boundary.usecase.domain.SaqueReturnInput
import com.kamila.saque.boundary.usecase.service.SaqueServiceInput
import com.kamila.saque.entity.Saque
import com.kamila.saque.entity.validateAmountWithAvailableNotes
import com.kamila.saque.usecase.mapper.toSaqueReturnInput
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class SaqueUseCase(
    private val qrCodeDatabase: QrCodeDatabase
) : SaqueServiceInput {

    private val logger = LoggerFactory.getLogger(javaClass)


    @Throws(ApplicationException::class, NotFoundException::class)
    override fun registerSaque(saqueInput: SaqueInput): SaqueReturnInput {
        try {
            val qrCode = saqueInput.qrCode.toQrCode()
            val saveQrCode = qrCodeDatabase.saveQrCode(qrCode)

            val saque = Saque(saqueInput.amount, qrCode)
            saque.validateAmountWithAvailableNotes()

            return saveQrCode.toSaqueReturnInput(saqueInput.amount)

        } catch (ex: Exception) {
            throw when (ex) {
                is ApplicationException -> ex
                else -> ApplicationException(
                    "server.error",
                    "[SAQUE] -> não foi possível salvar o QrCode - ${ex.message}", ex
                )
            }.also {
                logger.error("${LocalDateTime.now()} - ${it.code} - ${it.message}", it)
            }
        }
    }

}


