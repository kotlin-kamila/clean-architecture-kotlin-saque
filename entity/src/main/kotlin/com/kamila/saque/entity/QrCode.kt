package com.kamila.saque.entity

import com.kamila.saque.commons.exception.ApplicationException
import org.slf4j.LoggerFactory

data class QrCode(
    val fullText: String,
    val partner: String,
    val product: String,
    val uuid: String,
    val hash: String,
    val bankNotes: String,
    val notes: List<Int>?,
)

private val logger = LoggerFactory.getLogger(QrCode::class.java)

fun String.toQrCode(): QrCode {
    try {
        val items = this.getQrCodeItens()

        return QrCode(
            fullText = this,
            partner = items[0],
            product = items[1],
            uuid = items[2],
            hash = items[3],
            bankNotes = items[4],
            notes = items[4].getNotesList()
        )
    } catch (ex: Exception) {
        logger.error("${ex.message}", ex)
        throw ApplicationException("qrcode.invalid", ex.message)
    }
}

fun String.getNotesList(): List<Int> {
    return this
        .substringAfter("{")
        .substringBefore("}")
        .split(";")
        .map { it.toInt() }
}

private fun String.getQrCodeItens(): List<String> {
    return this.split("/")
}

