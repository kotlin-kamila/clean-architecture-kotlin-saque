package com.kamila.saque.entity

import com.kamila.saque.commons.exception.ApplicationException
import org.slf4j.LoggerFactory
import java.math.BigDecimal

private val logger = LoggerFactory.getLogger(Saque::class.java)

class Saque(
    val amount: BigDecimal,
    val qrCode: QrCode
)

fun Saque.validateAmountWithAvailableNotes() {
    val maxQuantityNotes = 20
    val mapAmountNotes = mutableMapOf<Int, Map<Int, Int>>()

    for (value in 10..amount.toInt() step 10) {
        qrCode.notes?.forEach { note ->
            val differenceValueNote = value - note

            if (differenceValueNote == 0) {
                mapAmountNotes[value] = mapOf(note to 1)
            }

            if (differenceValueNote > 0) {
                val differenceNotesList = mapAmountNotes[differenceValueNote]
                val sumDifferenceNote = differenceValueNote + note

                if (!differenceNotesList.isNullOrEmpty() && sumDifferenceNote == value) {
                    mapAmountNotes[value] = addNoteQuantityToList(differenceNotesList, note)
                }
            }
        }
    }
    this.verifyNotesCombinationAndNotesQuantityToAmount(mapAmountNotes, maxQuantityNotes)
}

private fun Saque.verifyNotesCombinationAndNotesQuantityToAmount(
    mapAmountNotes: MutableMap<Int, Map<Int, Int>>,
    maxQuantityNotes: Int
) {
    val listOfNotesCombinationToAmount = mapAmountNotes[amount.toInt()]

    if (!listOfNotesCombinationToAmount.isNullOrEmpty()) {
        val totalQuantityNotes = listOfNotesCombinationToAmount
            .map { it.value }
            .reduce { acc, item -> acc + item }

        if (totalQuantityNotes > maxQuantityNotes) {
            throw ApplicationException("notes.limit.invalid", "Não permitido saque acima de ${maxQuantityNotes} notas")
                .also {
                    logger.error("Não permitido saque acima de ${maxQuantityNotes} notas")
                }
        }
    } else {
        throw ApplicationException(
            "notes.invalid", "Terminal não possui notas para o saque no valor solicitado",
            qrCode.notes.toString()
                .replace("[", "")
                .replace("]", "")
        ).also {
            logger.error("Terminal não possui notas para o saque no valor solicitado")

        }
    }
}

fun addNoteQuantityToList(
    noteQuantity: Map<Int, Int>,
    note: Int
): Map<Int, Int> {
    val listOfNoteQuantityToAdd = noteQuantity.map { it.toPair() }
    val newNoteQuantity = noteQuantity.getOrDefault(note, 0) + 1

    return (listOfNoteQuantityToAdd.filter {
        it.first != note
    } + Pair(note, newNoteQuantity)).toMap()
}
