drop table if exists tb_qr_code;

CREATE TABLE tb_qr_code (
    id_qr_code int IDENTITY(1,1) PRIMARY KEY,
    tx_notes varchar(255),
    tx_hash varchar(255),
    tx_partner varchar(255),
    tx_product varchar(255),
    tx_qr_code varchar(255),
    tx_uuid varchar(255)
);

INSERT INTO tb_qr_code (id_qr_code, tx_qr_code)
VALUES (1, 'SaqueDigital/879ed45c-88ef-4c65-bfb2-b6d6f3dbd789/9be09eba/BRL{20;50;100}');