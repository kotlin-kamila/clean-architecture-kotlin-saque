package com.kamila.saque

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import java.util.*

@SpringBootApplication
@ComponentScan("com.kamila.saque")
class SaqueApplication

fun main(args: Array<String>) {
    TimeZone.setDefault(TimeZone.getTimeZone("GMT-3"))
    runApplication<SaqueApplication>(*args)
}
