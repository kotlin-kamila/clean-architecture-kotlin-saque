# Kotlin Clean Architecture Project

Exemplo de aplicação Spring Boot implementando Clean Architecture, utilizando a linguagem Kotlin.
Projeto separado em módulos.

Acesso ao banco de dados em `http://localhost:8080/h2-console`.
 - Host: jdbc:h2:mem:testdb
 - Senha: 123456

Usuario para autenticação em memória: 
 - User: admin
 - Senha 123456

Observação: A classe main no pacote "com.kamila.saque.boot" não compila, apenas no pacote "com.kamila.saque" até o momento. Dessa maneira o Spring consegue identificar 

### Criação de um módulo
Para isso estamos levando em consideração a utilização do `Gradle 6.9.2`, IDE Intellij.

1. Acessar `File > New > Module`
![File > New > Module](_images/new-module-1.png)

2. Selecionar `Gradle > Kotlin/JVM > Next`
![Gradle > Kotlin/JVM > Next](_images/new-module-2.png)

3. Insira o nome do módulo, "parent" insira o projeto principal, Finish
![Gradle > Kotlin/JVM > Next](_images/new-module-3.png)

4. Adicione a pasta `src`. Verifique se a inclusão do módulo foi adicionada ao arquivo `settings.gradle`:
> include 'repository'

5. Varifique se foi adicionado ao `build.gradle` do pacote `application`, que contém o método main:
```
dependencies {
       implementation project(":repository")
}
```

6. Construa a aplicação
> gradle clean build

#### Guide Spring
Creating a Multi Module Project: https://spring.io/guides/gs/multi-module/

## Developer
Kamila Serpa

[1]: https://www.linkedin.com/in/kamila-serpa/
[2]: https://gitlab.com/java-kamila

[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)][1]
[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)][2]
