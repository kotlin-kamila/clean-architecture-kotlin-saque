package com.kamila.saque.web.dto.response

import com.kamila.saque.boundary.usecase.domain.SaqueReturnInput
import java.math.BigDecimal

data class SaqueResponseDTO(
    val amount: BigDecimal
)


fun SaqueReturnInput.toSaqueResponseDTO() = SaqueResponseDTO(
    amount = amount
)
