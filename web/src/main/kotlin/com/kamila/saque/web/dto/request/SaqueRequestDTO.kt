package com.kamila.saque.web.dto.request

import com.kamila.saque.boundary.usecase.domain.SaqueInput
import io.swagger.annotations.ApiModelProperty
import java.math.BigDecimal
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

class SaqueRequestDTO(

    @field: NotNull
    @field: Positive
    val amount: BigDecimal,

    @ApiModelProperty(
        example = "SaqueDigital/879ed45c-88ef-4c65-bfb2-b6d6f3dbd789/9be09eba/BRL{20;50;100}"
    )
    @field: NotBlank
    val qrCode: String
)

fun SaqueRequestDTO.toInput() = SaqueInput(
    amount = amount,
    qrCode = qrCode
)
