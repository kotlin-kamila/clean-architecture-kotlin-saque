package com.kamila.saque.web.exception

import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.kamila.saque.commons.exception.ApplicationException
import com.kamila.saque.web.dto.response.ErrorDTO
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.*

@ControllerAdvice
class ExceptionHandler(val messageSource: MessageSource) {

    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(value = [ApplicationException::class])
    fun internalServerError(ex: ApplicationException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ErrorDTO(
                code = ex.code,
                message = resolveMessage(ex.code, null, ex.args)
            ),
            HttpStatus.BAD_REQUEST
        ).also { logger.error("${it.body?.code} - ${it.body?.message}") }
    }

    @ExceptionHandler(value = [MethodArgumentNotValidException::class])
    fun methodArgumentNotValidException(ex: MethodArgumentNotValidException): ResponseEntity<ErrorDTO> {
        val errorMessage = ex.bindingResult.allErrors
            .joinToString(" - ") { objectError ->
                val message = messageSource.getMessage(objectError, Locale.getDefault())
                var name = objectError.objectName
                if (objectError is FieldError) {
                    name = objectError.field
                }
                "$name: $message"
            }
        logger.error(errorMessage)

        return ResponseEntity(
            ErrorDTO(
                code = "argument.invalid",
                message = resolveMessage("server.error")
            ),
            HttpStatus.BAD_REQUEST
        ).also {
            logger.error("${it.body?.code} - ${it.body?.message}")
        }
    }

    @ExceptionHandler(value = [HttpMessageNotReadableException::class])
    fun httpMessageNotReadableException(ex: HttpMessageNotReadableException): ResponseEntity<ErrorDTO> {
        if (ex.rootCause is MissingKotlinParameterException) {
            return handleMissingKotlinParameterException(ex.rootCause as MissingKotlinParameterException)
        }
        if (ex.rootCause is InvalidFormatException) {
            return handleInvalidFormatException(ex.rootCause as InvalidFormatException)
        }

        return ResponseEntity(
            ErrorDTO(
                code = "argument.invalid",
                message = resolveMessage("server.error")
            ),
            HttpStatus.BAD_REQUEST
        ).also {
            logger.error("${it.body?.code} - ${it.body?.message}")
        }
    }

    private fun handleInvalidFormatException(ex: InvalidFormatException): ResponseEntity<ErrorDTO> {
        logger.error("A propriedade ${ex.path[0]?.fieldName} recebeu valor ${ex.value}, incompatível com o tipo ${ex.targetType.simpleName}).")
        return ResponseEntity(
            ErrorDTO(
                code = "argument.invalid",
                message = resolveMessage("server.error")
            ),
            HttpStatus.BAD_REQUEST
        ).also {
            logger.error("${it.body?.code} - ${it.body?.message}")
        }
    }

    private fun handleMissingKotlinParameterException(ex: MissingKotlinParameterException): ResponseEntity<ErrorDTO> {
        logger.error("A propriedade ${ex.parameter.name} do tipo ${ex.parameter.type} está inválida (campo ${ex.path[0]?.fieldName}).")
        return ResponseEntity(
            ErrorDTO(
                code = "argument.invalid",
                message = resolveMessage("server.error")
            ),
            HttpStatus.BAD_REQUEST
        ).also {
            logger.error("${it.body?.code} - ${it.body?.message}")
        }
    }

    private fun resolveMessage(
        message: String,
        defaultMessage: String? = null,
        args: Array<out String>? = null
    ): String =
        messageSource.getMessage(
            message,
            args,
            defaultMessage ?: "server.error",
            Locale.getDefault()
        )!!

}