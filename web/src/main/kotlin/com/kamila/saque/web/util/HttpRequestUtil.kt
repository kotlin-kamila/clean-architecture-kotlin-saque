package com.kamila.saque.web.util

import com.kamila.saque.commons.exception.ApplicationException
import org.springframework.util.LinkedCaseInsensitiveMap
import java.util.*
import javax.servlet.http.HttpServletRequest

private const val REMOTE_ADDR = "remoteAddr"
private const val CORRELATION_ID = "x.correlation.id"

fun HttpServletRequest.getAllHeaders(): Map<String, List<String>> {

    val headers = LinkedCaseInsensitiveMap<List<String>>(Locale.ENGLISH)
    val names = headerNames

    while (names.hasMoreElements()) {
        val name = names.nextElement()
        headers[name] = Collections.list(getHeaders(name)!!)
    }

    headers[REMOTE_ADDR] = listOf(REMOTE_ADDR)
    return headers
}

fun Map<String, List<String>>.getCorrelationID(isMandatory: Boolean): String? {
    return when {
        containsKey(CORRELATION_ID) && getValue(CORRELATION_ID)[0].isNotEmpty() -> {
            return getValue(CORRELATION_ID)[0]
        }
        isMandatory -> {
            throw ApplicationException("correlation.notfound")
        }
        else -> null
    }
}