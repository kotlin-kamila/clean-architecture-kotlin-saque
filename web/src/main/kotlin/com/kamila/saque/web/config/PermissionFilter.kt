package com.kamila.saque.web.config

import org.slf4j.MDC
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class PermissionFilter(authManager: AuthenticationManager) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        SecurityContextHolder.getContext().authentication =
            UsernamePasswordAuthenticationToken(
                request.getHeader("clientDocument") ?: UUID.randomUUID().toString(),
                null,
                request.getHeader("userPermissions")
                    ?.split(",")
                    ?.map { SimpleGrantedAuthority("ROLE_$it") }
        )

        MDC.put("x.correlation.id", request.getHeader("x.correlation.id"))
        MDC.put("clientDocument", request.getHeader("clientDocument"))
        MDC.put("account", request.getHeader("account"))

        filterChain.doFilter(request, response)
    }

}
