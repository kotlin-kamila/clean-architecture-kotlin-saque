package com.kamila.saque.web.config

import com.fasterxml.classmate.TypeResolver
import com.kamila.saque.web.dto.response.ErrorDTO
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RequestMethod
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.builders.ResponseMessageBuilder
import springfox.documentation.schema.ModelRef
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.ResponseMessage
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.paths.RelativePathProvider
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import javax.servlet.ServletContext

@Configuration
@EnableSwagger2
class SwaggerConfig(
    private val servletContext: ServletContext,
    @Value("\${swagger.host}") private val swaggerHost: String,
    private val environment: Environment
) {

    private val BASE_PACKAGE = "com.kamila.saque.web.controller"
    private val APP_BASE_PATH = "/saque"
    private val typeResolver = TypeResolver()

    @Bean
    fun api() =
        if (!environment.activeProfiles.contains("production")) {


            Docket(DocumentationType.SWAGGER_2)
                .host(swaggerHost)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .globalResponseMessage(RequestMethod.GET, globalResponse())
                .globalResponseMessage(RequestMethod.POST, globalResponse())
                .additionalModels(typeResolver.resolve(ErrorDTO::class.java))
                .apiInfo(apiInfo())
        } else {
            Docket(DocumentationType.SWAGGER_2)
                .host(swaggerHost)
                .pathProvider(
                    CustomPathProvider(servletContext, APP_BASE_PATH)
                )
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
        }

    private fun globalResponse(): MutableList<ResponseMessage> {
        return mutableListOf(
            ResponseMessageBuilder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message("Requisição inválida")
                .responseModel(ModelRef("ErrorDTO"))
                .build(),
            ResponseMessageBuilder()
                .code(HttpStatus.UNAUTHORIZED.value())
                .message("Unauthorized")
                .build()
        )
    }

    private fun apiInfo(): ApiInfo? {
        return ApiInfoBuilder()
            .title("Saque Clean Architecture")
            .description("API de exemplo de estrutura Clean Architecture")
            .build()
    }


}

class CustomPathProvider(servletContext: ServletContext, private val appBasePath: String) :
    RelativePathProvider(servletContext) {

    override fun getApplicationBasePath(): String = appBasePath
}
