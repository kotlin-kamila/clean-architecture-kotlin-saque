package com.kamila.saque.web.controller

import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Api(tags = ["Health"], description = "Response health")
class HealthController {

    @Value("\${GITVERSION:}")
    var Version = ""

    @GetMapping("/status")
    fun health(): HashMap<String, String> {
        return hashMapOf("status" to "ALIVE", "Version" to Version)
    }

}