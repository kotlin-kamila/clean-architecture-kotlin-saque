package com.kamila.saque.web.serializer

import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiImplicitParams

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.ANNOTATION_CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@ApiImplicitParams(
    ApiImplicitParam(name = "token", required = false, dataType = "string", paramType = "header"),
    ApiImplicitParam(name = "account", required = true, dataType = "string", paramType = "header"),
    ApiImplicitParam(name = "agency", required = true, dataType = "string", paramType = "header"),
    ApiImplicitParam(name = "bank", required = true, dataType = "string", paramType = "header"),
    ApiImplicitParam(name = "clientDocument", required = true, dataType = "string", paramType = "header"),
    ApiImplicitParam(name = "x.correlation.id", required = false, dataType = "string", paramType = "header"),
    ApiImplicitParam(name = "userPermissions", required = false, paramType = "header", example = "WITHDRAW")
)
annotation class GatewayHeaders