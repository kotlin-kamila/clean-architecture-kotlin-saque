package com.kamila.saque.web.dto.response

import io.swagger.annotations.ApiModel

@ApiModel("ErrorDTO")
data class ErrorDTO(
    val code: String,
    val message: String
)
