package com.kamila.saque.web.config

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder


//@Order(4)
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
//@EnableGlobalMethodSecurity(securedEnabled = true)
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
            .withUser("admin")
            .password(passwordEncoder().encode("123456"))
            .roles("ADMIN")
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
            .httpBasic()
            .and()
            .authorizeRequests()
            .antMatchers(
                "/",
                "/h2-console/**",
                "/swagger-resources/**",
                "/swagger-ui.html",
                "/v2/api-docs",
                "/webjars/**"
            ).permitAll()
            .anyRequest().authenticated()

            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

            .and()
            .cors()
            .and()
            .csrf().disable()
            .formLogin().disable()
            .addFilter(
                PermissionFilter(authenticationManager())
            )



        http.exceptionHandling().accessDeniedHandler { _, response, accessDeniedException ->
            response.status = HttpStatus.UNAUTHORIZED.value()
            response.writer.println("Invalid token")
            logger.error(accessDeniedException.message, accessDeniedException)
        }

        http.exceptionHandling().authenticationEntryPoint() { _, response, authEnception ->
            response.status = HttpStatus.UNAUTHORIZED.value()
            response.writer.println("Invalid token")
            logger.error(authEnception.message, authEnception)
        }

        http.headers().frameOptions().sameOrigin()
        http.headers().cacheControl() // disable page caching

    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }
}