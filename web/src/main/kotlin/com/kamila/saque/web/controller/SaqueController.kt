package com.kamila.saque.web.controller

import com.kamila.saque.commons.exception.ApplicationException
import com.kamila.saque.commons.exception.NotFoundException
import com.kamila.saque.boundary.usecase.service.SaqueServiceInput
import com.kamila.saque.web.dto.request.SaqueRequestDTO
import com.kamila.saque.web.dto.request.toInput
import com.kamila.saque.web.dto.response.SaqueResponseDTO
import com.kamila.saque.web.dto.response.toSaqueResponseDTO
import com.kamila.saque.web.serializer.GatewayHeaders
import com.kamila.saque.web.util.getAllHeaders
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@Api(tags = ["Saque"], description = "Api responsavel por saque")
@RequestMapping
class SaqueController(
    private val saqueService: SaqueServiceInput
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    //    @Secured("ROLE_SAQUE")
    @GatewayHeaders
    @PostMapping("saque")
    @ApiOperation(value = "Endpoint responsible to make saque")
    fun registerSaque(
        @Valid @RequestBody saque: SaqueRequestDTO,
        httpServletRequest: HttpServletRequest
    ): SaqueResponseDTO {
        try {
            val headers = httpServletRequest.getAllHeaders()
            val saqueReturnInput = saqueService.registerSaque(saque.toInput())

            logger.info("Logging sleuth test...")
            return saqueReturnInput.toSaqueResponseDTO()

        } catch (ex: Exception) {

            throw when (ex) {
                is ApplicationException -> ex
                is NotFoundException -> ex
                else -> ApplicationException("server.error", ex.message, ex)
            }.also {
                logger.error("[SAQUE] -> Error on call registerSaque - $saque", it)
            }
        }
    }

}


