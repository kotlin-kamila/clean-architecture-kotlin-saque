package com.kamila.saque.commons.exception

class ApplicationException : Exception {

    var code: String
    var args: Array<out String>? = null

    constructor(code: String) : super(code) {
        this.code = code
    }

    constructor(code: String, message: String?) : super(message) {
        this.code = code
    }

    constructor(code: String, message: String?, ex: Exception?) : super(message, ex) {
        this.code = code
    }

    constructor(code: String, message: String?, vararg args: String) : super(message) {
        this.code = code
        this.args = args
    }

    constructor(code: String, message: String, vararg args: String, ex: Exception?) : super(message, ex) {
        this.code = code
        this.args = args
    }

}
