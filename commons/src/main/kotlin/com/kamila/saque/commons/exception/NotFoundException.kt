package com.kamila.saque.commons.exception

class NotFoundException(val code: String, message: String) : Exception(message)
