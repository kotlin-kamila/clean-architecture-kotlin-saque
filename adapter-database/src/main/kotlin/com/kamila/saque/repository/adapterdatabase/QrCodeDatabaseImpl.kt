package com.kamila.saque.repository.adapterdatabase

import com.kamila.saque.repository.adapterdatabase.model.toEntity
import com.kamila.saque.repository.adapterdatabase.model.toModel
import com.kamila.saque.repository.adapterdatabase.repository.QrCodeRepository
import com.kamila.saque.commons.exception.ApplicationException
import com.kamila.saque.boundary.database.QrCodeDatabase
import com.kamila.saque.entity.QrCode
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class QrCodeDatabaseImpl(
    private val qrCodeRepository: QrCodeRepository
) : QrCodeDatabase {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun saveQrCode(qrCode: QrCode): QrCode {
        try {
            return qrCodeRepository.save(qrCode.toModel())
                .toEntity()

        } catch (ex: Exception) {
            throw when (ex) {
                is ApplicationException -> ex
                else -> ApplicationException(
                    "server.error",
                    "[SAQUE] -> não foi possível salvar o QrCode - ${ex.message}", ex
                )
            }.also {
                logger.error("${LocalDateTime.now()} - ${it.code} - ${it.message}", it)
            }
        }

    }

}
