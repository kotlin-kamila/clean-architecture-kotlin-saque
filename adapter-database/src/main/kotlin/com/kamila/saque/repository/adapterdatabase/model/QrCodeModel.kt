package com.kamila.saque.repository.adapterdatabase.model

import com.kamila.saque.entity.QrCode
import com.kamila.saque.entity.getNotesList
import javax.persistence.*

@Entity
@Table(name = "tb_qr_code")
class QrCodeModel(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_qr_code")
    val id: Long = 0L,

    @Column(name = "tx_qr_code")
    val qrCode: String = "",

    @Column(name = "tx_partner")
    val partner: String,

    @Column(name = "tx_product")
    val product: String,

    @Column(name = "tx_uuid")
    val uuid: String,

    @Column(name = "tx_hash")
    val hash: String,

    @Column(name = "tx_notes")
    val bankNotes: String
)

fun QrCode.toModel() = QrCodeModel(
    qrCode = fullText,
    partner = partner,
    product = product,
    uuid = uuid,
    hash = hash,
    bankNotes = bankNotes
)

fun QrCodeModel.toEntity() = QrCode(
    fullText = qrCode,
    partner = partner,
    product = product,
    uuid = uuid,
    hash = hash,
    bankNotes = bankNotes,
    notes = bankNotes.getNotesList()
)
