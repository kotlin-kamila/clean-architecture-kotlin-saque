package com.kamila.saque.repository.adapterdatabase.repository

import com.kamila.saque.repository.adapterdatabase.model.QrCodeModel
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface QrCodeRepository : CrudRepository<QrCodeModel, Long>